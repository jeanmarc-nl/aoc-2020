package nl.about42.aoc2020

import scala.collection.mutable
import scala.collection.mutable.Queue
import scala.io.Source

object day09 extends App {
  val source = Source.fromFile("src/main/resources/aoc2020_09_input.txt")
  val input = source.getLines().toList
  source.close()

  val values = input.map(_.toLong)
  val allowedValues = mutable.Queue.from(values.take(25))
  val remainder = mutable.Queue.from(values.drop(25))

  println(s"First 25 values are ${values.take(25)}")
  println(s"allowed values are $allowedValues}")

  var illegalEntry: Long = 0
  var done: Boolean = false
  while (!done) {
    if (remainder.isEmpty) {
      println("all entries processed")
      done = true
    }
    var entry = remainder.dequeue()
    if (isValid(entry, allowedValues)) {
      allowedValues.dequeue()
      allowedValues.enqueue(entry)
    } else {
      println(
        s"found illegal element $entry, it cannot be made from $allowedValues"
      )
      illegalEntry = entry
      done = true
    }
  }

  // now find a sequence in the original input that sums to the illegalEntry
  var start = 0
  var finish = 0
  var currentSum = values.head
  while (currentSum != illegalEntry) {
    if (currentSum < illegalEntry) {
      // add a number
      finish += 1
      currentSum += values(finish)
    } else if (currentSum > illegalEntry) {
      // remove a number
      currentSum -= values(start)
      start += 1
    }
  }
  println(
    s"Found a range $start - $finish, looking for lowest and highest value"
  )

  val range = values.slice(start, finish + 1)
  println(
    s"Min is ${range.min}, max is ${range.max}, sum is ${range.min + range.max}"
  )

  def isValid(entry: Long, allowedValues: Queue[Long]): Boolean = {
    var i: Int = 0
    var j: Int = 0
    var done: Boolean = false
    while (!done && i < allowedValues.size - 1) {
      j = i + 1
      while (!done && j < allowedValues.size) {
        if (allowedValues(i) + allowedValues(j) == entry) {
          done = true
        }
        j += 1
      }
      i += 1
    }
    done
  }
}
