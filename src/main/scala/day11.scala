package nl.about42.aoc2020

import scala.io.Source

sealed trait Angle
case object N extends Angle
case object NE extends Angle
case object E extends Angle
case object SE extends Angle
case object S extends Angle
case object SW extends Angle
case object W extends Angle
case object NW extends Angle

sealed trait Element {
  val emptyCount: Int
  val fullCount: Int
}
case object Occupied extends Element {
  val emptyCount: Int = 0
  val fullCount: Int = 1
  override def toString() = "#"
}
case object Empty extends Element {
  val emptyCount: Int = 1
  val fullCount: Int = 0
  override def toString() = "L"
}
case object Floor extends Element {
  val emptyCount: Int = 0
  val fullCount: Int = 0
  override def toString() = "."
}

class Occupancy(
    val width: Int,
    val height: Int,
    val plan: Array[Array[Element]]
) {
  override def toString() = {
    val res: StringBuilder = new StringBuilder
    plan.foreach(r => { r.foreach(res.append(_)); res.append("\n") })
    res.toString()
  }
  def occupiedSeats: Int = {
    plan.map(r => r.foldLeft(0)(_ + _.fullCount)).sum
  }

  def directNeighbourCount(row: Int, col: Int): Int = {
    val neighbours = Seq(
      (row - 1, col - 1),
      (row - 1, col),
      (row - 1, col + 1),
      (row, col - 1),
      (row, col + 1),
      (row + 1, col - 1),
      (row + 1, col),
      (row + 1, col + 1)
    ).filter(_._1 >= 0)
      .filter(_._1 < height)
      .filter(_._2 >= 0)
      .filter(_._2 < width)

    val result = neighbours.map(rc => plan(rc._1)(rc._2).fullCount).sum
//    println(
//      s"for $row, $col we looked at $neighbours and have $result seats occupied"
//    )
    result
  }

  def visibleNeighbourCount(row: Int, col: Int): Int = {
    val angles = List(N, NE, E, SE, S, SW, W, NW)

    angles.map(occupiedChairVisible(row, col, _)).count(x => x)
  }

  def occupiedChairVisible(row: Int, col: Int, direction: Angle): Boolean = {
    direction match {
      case N =>
        var neighborRow = row - 1

        while (neighborRow >= 0) {
          plan(neighborRow)(col) match {
            case Occupied => return true
            case Empty    => return false
            case _        => // continue looking
          }
          neighborRow -= 1
        }
        false

      case NE =>
        var neighborRow = row - 1
        var neighborCol = col + 1

        while (neighborRow >= 0 && neighborCol < width) {
          plan(neighborRow)(neighborCol) match {
            case Occupied => return true
            case Empty    => return false
            case _        => // continue looking
          }
          neighborRow -= 1
          neighborCol += 1
        }
        false

      case E =>
        var neighborCol = col + 1

        while (neighborCol < width) {
          plan(row)(neighborCol) match {
            case Occupied => return true
            case Empty    => return false
            case _        => // continue looking
          }
          neighborCol += 1
        }
        false

      case SE =>
        var neighborRow = row + 1
        var neighborCol = col + 1

        while (neighborRow < height && neighborCol < width) {
          plan(neighborRow)(neighborCol) match {
            case Occupied => return true
            case Empty    => return false
            case _        => // continue looking
          }
          neighborRow += 1
          neighborCol += 1
        }
        false

      case S =>
        var neighborRow = row + 1

        while (neighborRow < height) {
          plan(neighborRow)(col) match {
            case Occupied => return true
            case Empty    => return false
            case _        => // continue looking
          }
          neighborRow += 1
        }
        false

      case SW =>
        var neighborRow = row + 1
        var neighborCol = col - 1

        while (neighborRow < height && neighborCol >= 0) {
          plan(neighborRow)(neighborCol) match {
            case Occupied => return true
            case Empty    => return false
            case _        => // continue looking
          }
          neighborRow += 1
          neighborCol -= 1
        }
        false

      case W =>
        var neighborCol = col - 1

        while (neighborCol >= 0) {
          plan(row)(neighborCol) match {
            case Occupied => return true
            case Empty    => return false
            case _        => // continue looking
          }
          neighborCol -= 1
        }
        false

      case NW =>
        var neighborRow = row - 1
        var neighborCol = col - 1

        while (neighborRow >= 0 && neighborCol >= 0) {
          plan(neighborRow)(neighborCol) match {
            case Occupied => return true
            case Empty    => return false
            case _        => // continue looking
          }
          neighborRow -= 1
          neighborCol -= 1
        }
        false

    }
  }
}

class Grid(p1: Occupancy, p2: Occupancy) {
  val width = p1.width
  val height = p1.height
  private var current = 0
  private var next = 1
  private var iteration = 0
  val plans: Array[Occupancy] = Array(p1, p2)

  def getIteration: Int = iteration
  def getPlan: Occupancy = plans(current)
  def nextIteration(): Boolean = {

    (0 to height - 1).foreach(row => {
      (0 to width - 1).foreach(col => {
        val occupiedNeighbours = plans(current).visibleNeighbourCount(row, col)
        plans(current).plan(row)(col) match {
          case Empty =>
            if (occupiedNeighbours == 0) {
              plans(next).plan(row).update(col, Occupied)
            } else {
              plans(next).plan(row).update(col, Empty)
            }
          case Occupied =>
            if (occupiedNeighbours >= 5) {
              plans(next).plan(row).update(col, Empty)
            } else {
              plans(next).plan(row).update(col, Occupied)
            }
          case _ => // no action
        }
      })
    })

    next = 1 - next
    current = 1 - next
    iteration += 1
    isStable
  }

  def isStable: Boolean = {
    // compare both plans to see if they are identical
    val p0 = plans(0).toString()
    val p1 = plans(1).toString()
    p0.equals(p1)
  }
}

object day11 extends App {
  val source = Source.fromFile("src/main/resources/aoc2020_11_input.txt")
  //val source = Source.fromFile("src/main/resources/11_small.txt")
  val input = source.getLines().toList
  source.close()

  val p1 = buildOccupancy(input)
  val p2 = buildOccupancy(input)
  println(s"$p1")

  val theGrid = new Grid(p1, p2)

  println(s"at the start isStable == ${theGrid.isStable}")
  println(s"at the start, occupiedSeats == ${p1.occupiedSeats}")

  var done = false
  while (!done) {
    theGrid.nextIteration()
    println(s"iteration ${theGrid.getIteration}:\n${theGrid.getPlan}")
    done = theGrid.isStable
  }
  println(s"Stable grid has ${theGrid.getPlan.occupiedSeats} occupied seats")

  def buildOccupancy(lines: List[String]): Occupancy = {
    val width = lines(0).size
    val height = lines.size
    val plan: Array[Array[Element]] = lines.toArray.map(l =>
      l.toArray.map {
        case '.' => Floor
        case 'L' => Empty
        case '#' => Occupied
      }
    )
    new Occupancy(width, height, plan)
  }

}
