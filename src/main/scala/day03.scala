package nl.about42.aoc2020

import scala.io.Source

case class JourneyStep(pos: Int, isTree: Boolean)

object day03 extends App {
  val input =
    Source.fromFile("src/main/resources/aoc2020_03_input.txt").getLines().toList
  //Source.fromFile("src/main/resources/smallForest.txt").getLines().toList

  val width = input(0).size

  println(s"Got a forest of $width by ${input.size}")

  var pos = 0

  var journey = Seq[JourneyStep]()

  journey = journey :+ JourneyStep(0, false)

  journey = travel(JourneyStep(0, false), input.tail, 3, 1, journey)

  val treeCount = journey.map(_.isTree).count(x => x)

  println(s"We've hit ${treeCount} trees on our ${journey.size} step travel")

  val j1 = travel(JourneyStep(0, false), input.tail, 1, 1, Seq.empty)
  val j2 = travel(JourneyStep(0, false), input.tail, 3, 1, Seq.empty)
  val j3 = travel(JourneyStep(0, false), input.tail, 5, 1, Seq.empty)
  val j4 = travel(JourneyStep(0, false), input.tail, 7, 1, Seq.empty)
  val j5 = travel(JourneyStep(0, false), input.tail, 1, 2, Seq.empty)

  val tc1 = j1.map(_.isTree).count(x => x).toLong
  val tc2 = j2.map(_.isTree).count(x => x).toLong
  val tc3 = j3.map(_.isTree).count(x => x).toLong
  val tc4 = j4.map(_.isTree).count(x => x).toLong
  val tc5 = j5.map(_.isTree).count(x => x).toLong

  println(
    s"The five journeys have $tc1, $tc2, $tc3, $tc4, and $tc5 steps, which are multiplied to ${tc1 * tc2 * tc3 * tc4 * tc5}"
  )

  def travel(
      start: JourneyStep,
      forest: List[String],
      right: Int,
      down: Int,
      journey: Seq[JourneyStep]
  ): Seq[JourneyStep] = {
    if (forest.size > down - 1) {
      val nextStep = step(start.pos, right, down, forest)
      if (down == 2)
        travel(nextStep, forest.tail.tail, right, down, journey :+ nextStep)
      else
        travel(nextStep, forest.tail, right, down, journey :+ nextStep)
    } else {
      journey
    }
  }

  def step(
      start: Int,
      right: Int,
      down: Int,
      forest: List[String]
  ): JourneyStep = {
    val newPos = (start + right) % width
    val landOnTree = forest(down - 1).charAt(newPos) == '#'
    val result = JourneyStep(newPos, landOnTree)
    //println(s"$start plus $right in ${forest(0)} tree is $landOnTree")
    result
  }
}
