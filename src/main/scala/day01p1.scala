package nl.about42.aoc2020

import scala.io.Source

object day01p1 extends App {
  val input = Source.fromFile("src/main/resources/aoc2020_01_input.txt")

  val amounts = input.getLines().toList.map(_.toInt)

  val (a, b) =  findMatch(amounts.head, 2020, amounts.tail)
  println(s"Found $a and $b, with sum  ${a + b} and product ${ a * b }")

  def findMatch(first: Int, target: Int, list: List[Int]): (Int, Int) = {
    if (list.contains(target - first)){
      (first, target - first)
    } else {
      findMatch(list.head, target, list.tail)
    }
  }
}

