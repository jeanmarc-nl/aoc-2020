package nl.about42.aoc2020

import scala.io.Source

object day05 extends App {
  val source = Source.fromFile("src/main/resources/aoc2020_05_input.txt")
  //val source = Source.fromFile("src/main/resources/small05.txt")
  val input = source.getLines().toList
  source.close()

  val rowsAndSeats = input.map(x => (x.take(7), x.drop(7)))

  println(s"We have ${rowsAndSeats.size} passes")

  val numbered = rowsAndSeats
    .map(x => (rowNum(x._1), seatNum(x._2), x))
    .map(x => (x._1 * 8 + x._2, x))

  val max = numbered.maxBy(_._1)

  println(s"Highest ID is $max")

  val allIds = numbered.map(_._1).sorted

  // find first gap
  val mySeat = findGap(allIds)
  println(s"Found a gap: seat id $mySeat")

  def findGap(ids: List[Int]): Int = {
    if (ids(0) + 1 != ids(1)) {
      ids(0) + 1
    } else {
      findGap(ids.tail)
    }
  }

  def rowNum(row: String): Int = {
    value(row(0), 64, 'B') +
      value(row(1), 32, 'B') +
      value(row(2), 16, 'B') +
      value(row(3), 8, 'B') +
      value(row(4), 4, 'B') +
      value(row(5), 2, 'B') +
      value(row(6), 1, 'B')
  }

  def seatNum(seat: String): Int = {
    value(seat(0), 4, 'R') +
      value(seat(1), 2, 'R') +
      value(seat(2), 1, 'R')
  }

  def value(in: Char, value: Int, highChar: Char): Int = {
    if (in == highChar) value else 0
  }
}
