package nl.about42.aoc2020

import scala.io.Source

object day01p2 extends App {
  val input = Source.fromFile("src/main/resources/aoc2020_01_input.txt")

  val amounts = input.getLines().toList.map(_.toInt)

  val (a, b, c) =  findTripleMatch(amounts.head, amounts.tail.head, 2020, amounts.tail.tail)
  println(s"Found $a, $b, and $c, with sum  ${a + b + c} and product ${ a * b * c}")


  def findTripleMatch(first: Int, second: Int, target: Int, list: List[Int]): (Int, Int, Int) = {

    findMatch(second, target-first, list) match  {
      case Some(x) => (first, x._1, x._2)
      case None => findTripleMatch(second, list.head, target, list.tail)
    }
  }

  def findMatch(first: Int, target: Int, list: List[Int]): Option[(Int, Int)] = {
    if (list.contains(target - first)){
      return Some((first, target - first))
    } else {
      if (!list.isEmpty)
        return findMatch(list.head, target, list.tail)
    }
    None
  }
}
