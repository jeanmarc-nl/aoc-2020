package nl.about42.aoc2020

import scala.io.Source

object day04 extends App {
  case class Entry(kv: Map[String, String])

  //val source = Source.fromFile("src/main/resources/04validAndInvalid.txt")
  val source = Source.fromFile("src/main/resources/aoc2020_04_input.txt")
  val input = source.getLines().toList
  source.close()

  val entries = parse(input)

  val hasAllFieldsCount = entries.map(isValid).count(x => x)
  println(
    s"We found ${entries.size} documents, of which $hasAllFieldsCount have all required fields"
  )

  val entriesWithAllFields = entries.filter(isValid)

  val validCount = entriesWithAllFields.map(meetsFieldValidations).count(x => x)
  println(
    s"We found ${entries.size} documents, of which $validCount meet all requirements"
  )

  def meetsFieldValidations(entry: Entry): Boolean = {
    val result = isValidByr(entry.kv("byr")) &&
      isValidIyr(entry.kv("iyr")) &&
      isValidEyr(entry.kv("eyr")) &&
      isValidHgt(entry.kv("hgt")) &&
      isValidHcl(entry.kv("hcl")) &&
      isValidEcl(entry.kv("ecl")) &&
      isValidPid(entry.kv("pid"))
    if (!result) println(s"$result for $entry")
    result
  }

  def isValidByr(in: String) = {
    val result = in.matches("""\d\d\d\d""") &&
      in.toInt >= 1920 &&
      in.toInt <= 2002
    if (!result) println(s"byr $in is $result")
    result
  }

  def isValidEyr(in: String) = {
    val result = in.matches("""\d\d\d\d""") &&
      in.toInt >= 2020 &&
      in.toInt <= 2030
    if (!result) println(s"eyr $in is $result")
    result

  }

  def isValidIyr(in: String) = {
    val result = in.matches("""\d\d\d\d""") &&
      in.toInt >= 2010 &&
      in.toInt <= 2020
    if (!result) println(s"iyr $in is $result")
    result

  }

  def isValidEcl(in: String) = {
    val validColors = Set("amb", "blu", "brn", "gry", "grn", "hzl", "oth")
    val result = validColors.contains(in)
    if (!result) println(s"ecl $in is $result")
    result
  }

  def isValidHcl(in: String) = {
    val result = in.matches("""#[0-9a-f]{6}""")
    if (!result) println(s"hcl $in is $result")
    result
  }

  def isValidHgt(in: String) = {
    val result =
      if (
        in.matches("""\d{3}cm""") ||
        in.matches("""\d{2}in""")
      ) {
        val h = in.dropRight(2).toInt
        if (in.takeRight(2) == "cm") {
          150 <= h && h <= 193
        } else {
          59 <= h && h <= 76
        }
      } else {
        false
      }
    if (!result) println(s"hgt $in is $result")
    result
  }

  def isValidPid(in: String) = {
    val result = in.matches("""[0-9]{9}""")
    if (!result) println(s"pid $in is $result")
    result
  }

  def isValid(entry: Entry): Boolean = {
    val keys = entry.kv.keySet
    keys.contains("byr") &&
    keys.contains("iyr") &&
    keys.contains("eyr") &&
    keys.contains("hgt") &&
    keys.contains("hcl") &&
    keys.contains("ecl") &&
    keys.contains("pid")
  }

  def parse(input: List[String], agg: Seq[Entry] = Seq.empty): Seq[Entry] = {
    val (count, map) = getKVs(input)
    if (count < input.size) {
      parse(input.drop(count), agg :+ Entry(map))
    } else {
      agg :+ Entry(map)
    }
  }

  def getKVs(input: List[String]): (Int, Map[String, String]) = {
    var i = 0
    var kv: Map[String, String] = Map.empty
    while (i < input.size && input(i).trim != "") {
      val elems = input(i).split(" ")
      val kvs = elems.map(x => x.split(":"))
      kvs.foreach(pair => kv = kv + (pair(0) -> pair(1).trim))
      i = i + 1
    }
    (i + 1, kv)
  }
}
