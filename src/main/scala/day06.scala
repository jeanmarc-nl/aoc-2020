package nl.about42.aoc2020

import scala.io.Source

object day06 extends App {
  case class GroupAnswers(groupSize: Int, answers: Set[Char])

  val source = Source.fromFile("src/main/resources/aoc2020_06_input.txt")
  //val source = Source.fromFile("src/main/resources/small05.txt")
  val input = source.getLines().toList
  source.close()

  val answers = getUnionAnswers(input)

  println(s"There are ${answers.size} groups in the file")

  val answerCount = answers.foldLeft(0)(_ + _.answers.size)
  println(s"There are ${answerCount} answers (union)")

  val intersectAnswers = getIntersectionAnswers(input)
  println(s"There are ${intersectAnswers.size} groups in the file")

  val answerIntersectCount = intersectAnswers.foldLeft(0)(_ + _.answers.size)
  println(s"There are ${answerIntersectCount} answers (intersection)")

  def getUnionAnswers(
      lines: List[String],
      acc: List[GroupAnswers] = List.empty
  ): List[GroupAnswers] = {
    val groupAnswer = getGroupUnionAnswers(lines)
    if (groupAnswer.groupSize < lines.size) {
      getUnionAnswers(lines.drop(groupAnswer.groupSize), groupAnswer :: acc)
    } else {
      groupAnswer :: acc
    }
  }

  def getGroupUnionAnswers(lines: List[String]): GroupAnswers = {
    var answers: Set[Char] = Set.empty
    var i = 0
    while (i < lines.size && lines(i) != "") {
      lines(i).toList.foreach(c => answers = answers + c)
      i = i + 1
    }
    GroupAnswers(i + 1, answers)
  }

  def getIntersectionAnswers(
      lines: List[String],
      acc: List[GroupAnswers] = List.empty
  ): List[GroupAnswers] = {
    val groupAnswer = getGroupIntersectionAnswers(lines)
    if (groupAnswer.groupSize < lines.size) {
      getIntersectionAnswers(
        lines.drop(groupAnswer.groupSize),
        groupAnswer :: acc
      )
    } else {
      groupAnswer :: acc
    }
  }

  def getGroupIntersectionAnswers(lines: List[String]): GroupAnswers = {
    var answers: Set[Char] = lines(0).toList.toSet
    var i = 1
    while (i < lines.size - 1 && lines(i) != "") {
      val nextPersonAnswers = lines(i).toList.toSet
      answers = answers.intersect(nextPersonAnswers)
      i = i + 1
    }
    GroupAnswers(i + 1, answers)
  }
}
