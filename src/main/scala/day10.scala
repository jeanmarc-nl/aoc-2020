package nl.about42.aoc2020

import scala.io.Source

object day10 extends App {
  //val source = Source.fromFile("src/main/resources/10_tiny.txt")
  //val source = Source.fromFile("src/main/resources/10_small.txt")
  val source = Source.fromFile("src/main/resources/aoc2020_10_input.txt")
  val input = source.getLines().map(_.toInt).toList.sorted
  source.close()

  // we have to add 0 and max+3 to the list

  val all = input.appended(0).appended(input(input.size - 1) + 3).sorted
  val pairs = all.dropRight(1).zip(all.tail)

  println(s"$pairs")

  val diffs = pairs.map(p => p._2 - p._1)
  println(s"$diffs")

  val numOnes = diffs.filter(_ == 1).size
  val numTwos = diffs.filter(_ == 2).size
  val numThrees = diffs.filter(_ == 3).size

  println(
    s"There are $numOnes diffs of 1, $numTwos diffs of 2, and $numThrees diffs of 3 (incl device), product is ${numThrees * numOnes}"
  )

  val max = all(all.size - 1)
  println(s"The min is ${all(0)}, the max is $max")

  // find number of ways to reach the end

  val routeCount: Map[Int, Long] = process(all.tail, Map.empty + (0 -> 1L))

  println(s"There are ${routeCount.getOrElse(max, -1)} ways to reach the end")

  def process(
      adapters: List[Int],
      knownRoutes: Map[Int, Long]
  ): Map[Int, Long] = {
    val adapter = adapters.head
    val waysToReach: Long =
      (1 to 3).map(i => knownRoutes.get(adapter - i)).map(_.getOrElse(0L)).sum
    if (adapters.size == 1) {
      knownRoutes + (adapter -> waysToReach)
    } else {
      process(adapters.tail, knownRoutes + (adapter -> waysToReach))
    }
  }
}
