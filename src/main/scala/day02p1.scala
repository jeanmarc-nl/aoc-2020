package nl.about42.aoc2020

import scala.io.Source

case class Entry(min: Int, max: Int, char: Char, pass: String)

object day02p1 extends App {
  val input =
    Source.fromFile("src/main/resources/aoc2020_02_input.txt").getLines().toList

  val checkList = input.map(parse)

  val results = checkList.map(isValid).filter(x => x)

  println(
    s"There are ${results.size} valid passwords in the list of ${checkList.size} passwords"
  )

  def isValid(entry: Entry): Boolean = {
    val charCount = entry.pass.filter(x => x == entry.char).size

    entry.min <= charCount && charCount <= entry.max
  }

  def parse(in: String): Entry = {
    val parts = in.split(":")
    val policy = parts(0).split(" ")
    val sizes = policy(0).split("-")
    val result =
      Entry(sizes(0).toInt, sizes(1).toInt, policy(1).head, parts(1).trim)
    println(result)
    result
  }
}

object day02p2 extends App {
  val input =
    Source.fromFile("src/main/resources/aoc2020_02_input.txt").getLines().toList

  val checkList = input.map(parse)

  val results = checkList.map(isValid).filter(x => x)

  println(
    s"There are ${results.size} valid passwords in the list of ${checkList.size} passwords"
  )

  def isValid(entry: Entry): Boolean = {
    val p1 = entry.pass(entry.min - 1)
    val p2 = entry.pass(entry.max - 1)

    (p1, p2) match {
      case (entry.char, entry.char)                       => false
      case (a, b) if (a != entry.char && b != entry.char) => false
      case _                                              => true
    }
  }

  def parse(in: String): Entry = {
    val parts = in.split(":")
    val policy = parts(0).split(" ")
    val sizes = policy(0).split("-")
    val result =
      Entry(sizes(0).toInt, sizes(1).toInt, policy(1).head, parts(1).trim)
    result
  }
}
