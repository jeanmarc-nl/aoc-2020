package nl.about42.aoc2020

import scala.io.Source

object day08 extends App {
  val source = Source.fromFile("src/main/resources/aoc2020_08_input.txt")
  val input = source.getLines().toList
  source.close()

  case class Instruction(operand: String, argument: Int)

  val program: Array[Instruction] = input.map(parseLine).toArray

  val runFlags: Array[Boolean] = Array.fill(program.size) { false }

  var acc = 0
  var pointer = 0

  println(s"Program size ${program.length}, bool array ${runFlags.size}")

  var done = false
  while (!done && pointer < program.size) {
    if (runFlags(pointer)) {
      println(
        s"Second visit to instruction at $pointer. Accum is currently $acc"
      )
      done = true
    } else {
      runFlags.update(pointer, true)
      program(pointer) match {
        case Instruction("nop", _) => pointer += 1
        case Instruction("jmp", n) => pointer += n
        case Instruction("acc", n) =>
          acc += n
          pointer += 1
      }
    }
  }

  var i = 0
  done = false
  while (!done) {
    program(i) match {
      case Instruction("acc", _) => // no need to test
        i += 1
      case _ =>
        val result = tryRun(i)
        done = result._1
        acc = result._2
        i += 1
    }
  }

  println(s"Found a terminating solution by flipping ${i - 1}: acc is $acc")

  def tryRun(fixLine: Int): (Boolean, Int) = {
    program(fixLine) match {
      case Instruction("nop", n) =>
        val modProg = program.clone()
        modProg.update(fixLine, Instruction("jmp", n))
        testCompletion(0, 0, Set.empty, modProg)
      case Instruction("jmp", n) =>
        val modProg = program.clone()
        modProg.update(fixLine, Instruction("nop", n))
        testCompletion(0, 0, Set.empty, modProg)
    }
  }

  def testCompletion(
      pointer: Int,
      acc: Int,
      visited: Set[Int],
      program: Array[Instruction]
  ): (Boolean, Int) = {
    val (newPointer, newAcc) = handleInstruction(pointer, acc, program(pointer))
    if (newPointer == program.size) {
      (true, acc)
    } else if (visited.contains(newPointer)) {
      (false, acc)
    } else {
      testCompletion(newPointer, newAcc, visited + pointer, program)
    }
  }

  def handleInstruction(
      pointer: Int,
      acc: Int,
      instruction: Instruction
  ): (Int, Int) = {
    instruction match {
      case Instruction("nop", _) => (pointer + 1, acc)
      case Instruction("jmp", n) => (pointer + n, acc)
      case Instruction("acc", n) => (pointer + 1, acc + n)
    }
  }

  def parseLine(line: String): Instruction = {
    val parts = line.split(" ")
    Instruction(parts(0), parts(1).toInt)
  }
}
